<?php $pageTitle="FlixPipe || Registration"; ?>
<?php include('backend/shared/header.php'); ?>
    <header class="site-header signUpBasicHeader">
      <a href="index.php" class="brand-container" title="FlixPipe">
      <img src="/frontend/assets/images/flixpipe-v2.png" alt="FlixPipe Logo" class="site-logo">
        <span class="screen-reader-text">FlixPipe</span>
      </a>
      <a href="login.php" class="signInLink">Sign In</a>
    </header>
  <section class="simpleContainer">
    <div class="centerContainer">
      <form action="" class="action">
        <div class="regFormContainer">
          <div class="stepHeader-container">
            <h1 class="stepTitle">Create account to start watching FlixPipe</h1>
          </div>

          <div class="stepHeader-body">
            <div class="group">
              <label for="fname">First Name</label>
              <input type="text" id="fname" name=firstName autocomplete="off" required>
            </div>
            <div class="group">
              <label for="lname">Last Name</label>
              <input type="text" id="lname" name=lastName autocomplete="off" required>
            </div>
            <div class="group">
              <label for="email">Email</label>
              <input type="text" id="email" name=email autocomplete="off" required>
            </div>
            <div class="group">
              <label for="pwd">Password</label>
              <input type="text" id="pwd" name=pwd autocomplete="off" required>
            </div>
            <div class="group r-me">
              <input type="checkbox" id="remember" name=remember autocomplete="off" class="remember">
              <label for="remember">Remember me</label>
            </div>
          </div>

          <div class="sumbitBtnContainer">
            <button type="button" name="submitButton">Register</button>
          </div>

        </div>
      </form>
    </div>
  </section>

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</body>
</html>
