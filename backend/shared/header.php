<?php  
if(!isset($pageTitle)){
    $pageTitle="FlixPipe - Watch TV Shows & Movies Online";
}
?>
<!doctype html>
<html lang="en-us">
<head>
 <meta charset="UTF-8">
 <meta http-equiv="X-UA-Compatible" content="IE=Edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <title><?php echo $pageTitle; ?></title>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

<link rel="stylesheet" href="/frontend/assets/css/master.css">
<link rel="shortcut icon" href="/frontend/assets/images/favicon.ico" type="image/x-icon">
</head>
<body>
