<?php

class Account {
    private $pdo;
    private $errorArray=[];

    public function __construct() {
        $this->pdo=Database::instance();
    }

    public function validateEmail($em) {
        $stmt=$this->pdo->prepare("SELECT * FROM `users` WHERE email=:email");
        $stmt->bindParam(":email",$em,PDO::PARAM_STR);
        $stmt->execute();
        $count=$stmt->rowCount();
        if($count >0) {
            return array_push($this->errorArray,Constant::$emailInUse);
        }

        if(!filter_var($em,FILTER_VALIDATE_EMAIL)) {
            return array_push($this->errorArray,Constant::$emailInValid);
        }

        if(empty($this->errorArray)) {
            return $em;
        }else{
            return false;
        }
    }

    public function getErrorMessage($error) {
        if(in_array($error,$this->errorArray)) {
            return "<div class='f-group inputError'>$error</div>";
        }
    }
}