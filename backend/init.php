<?php 
ini_set('display_errors',1);
error_reporting(E_ALL);

ob_start();

include "backend/config.php";

// Autoload classes
spl_autoload_register(function($class){
    require_once "classes/{$class}.php";
});


session_start();

require "functions.php";

$account=new Account;

