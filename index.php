<?php 
include "backend/init.php";
include "backend/shared/header.php"; 
include "backend/classes/FormSanitizer.php";

if(is_post_request()) {
  if(isset($_POST['email']) && !empty($_POST['email'])) {
    $email=FormSanitizer::sanitizeFormEmail($_POST['email']);
    $em_validated=$account->validateEmail($email);

    if(is_int($em_validated)) {
      
    }else{
      session_regenerate_id();
      $_SESSION['email']=$em_validated;
      redirect_to(url_for('signup.php'));
    
    }
  }
}
 
?>

  <section class="site-container">
    <header class="site-header">
      <div class="brand-container">
        <img src="/frontend/assets/images/flixpipe-v2.png" alt="FlixPipe Logo" class="site-logo">
        <span class="screen-reader-text">FlixPipe</span>
      </div>
      <a href="login.php">Sign In</a>
    </header>
  </section>
  <section class="banner">
    <img src="/frontend/assets/images/movie-banner.jpg" alt="Site Banner" class="our-story-card-background">
    <div class="our-story-card-text">
      <div class="heading">
        <h2>Unlimited movies, TV shows, and more</h2>
        <h5>Watch anywhere. Cancel anytime.</h5>
        <p>Ready to watch? Enter your email to create or restart your membership.</p>
      </div>
      <form action="<?php echo h($_SERVER['PHP_SELF']); ?>" method="POST" class="global-search">
        <div class="f-group">
          <input type="email" name="email" placeholder="Email Address..." value="<?php echo getInputValue('email'); ?>" autocomplete="off" required>
          <button type="submit" name="submit">Get Started</button>
        </div>
      </form>
      <?php echo $account->getErrorMessage(Constant::$emailInUse); ?>
      <?php echo $account->getErrorMessage(Constant::$emailInValid); ?>
    </div>
  </section>

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</body>
</html>
